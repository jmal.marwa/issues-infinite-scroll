// ==UserScript==
// @name         GitLab infinite scroll
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Enable infinite scroll for issues
// @author       Florian Lemaitre
// @match        http*://*/*/issues*
// @grant        none
// ==/UserScript==
window.isLoadingPage = false;
window.$loadingPage = $('<div class="loading" style="display: none;"><i class="fa fa-spinner fa-spin"></i></div>');

(function() {
    if (! isGitLab()) {
        return;
    }

    var url = window.location.href;
    var current_page = 1;
    var last_page = 1;
    var last_page_link = false;
    if ($('ul.pagination').find('li.last').length) {
        last_page_link = $('ul.pagination').find('li.last').find('a');
    }
    else if ($('ul.pagination').find('li.page').length) {
        last_page_link = $('ul.pagination').find('li.page').last().find('a');
    }

    if (last_page_link) {
        $(last_page_link.attr('href').split('?')[1].split('&')).each(function(key, value) {
            if (value.split('=')[0] == 'page') {
                last_page = value.split('=')[1];
            }
        });
    }

    $('.content').append(window.$loadingPage);
    $('.gl-pagination').hide();

    $(window).scroll(function() {
        if(!window.isLoadingPage && ($(window).scrollTop() + $(window).height() > $(document).height() - 100)) {
            if (current_page < last_page) {
                loadPageContent(url, ++current_page);
            }
        }
    });
})();

function isGitLab() {
    return $('meta[content=GitLab]').length > 0;
}

function loadPageContent(url, page) {
    window.isLoadingPage = true;
    window.$loadingPage.show();
    var params = url.split('?').length > 1;
    var url_paginated = url + (params ? '&' : '?') + 'page=' + page;
    $.get(url_paginated, function( data ) {
        $('.issues-list').append($(data).find('.issues-list > li'));
        window.isLoadingPage = false;
        window.$loadingPage.hide();
    });
}